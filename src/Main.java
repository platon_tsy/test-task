import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.LongStream;

import static java.util.stream.Collectors.toList;

public class Main {

    public static void main(String[] args) throws IOException {
        ExecutorService executorService = Executors.newFixedThreadPool(10_000);
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/x-www-form-urlencoded");
        AtomicBoolean isTaskFailed = new AtomicBoolean(false);
        int threadsCount = 10_000;
        List<? extends Future<?>> tasks = LongStream
                .rangeClosed(0, threadsCount)
                .boxed()
                .map(taskNumber -> executorService.submit(new RequestTask(taskNumber, isTaskFailed, headers)))
                .collect(toList());
        executorService.shutdown();
    }

}
