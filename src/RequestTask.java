import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;


public class RequestTask implements Runnable, IRequest {

    private int range = 100;
    private String link = "http://www.rollshop.co.il/test.php";
    private AtomicBoolean isTaskFailed;
    private long start;
    private Map<String, String> headers;
    private static final int TIMEOUT = 1000 * 30 * 60;
    private static final String METHOD = "POST";
    private static final String RESULT_FILENAME = "result.txt";
    private static final String WRONG_RESULT = "WRONG =(";

    public RequestTask(long number, AtomicBoolean isTaskFailed, Map<String, String> headers) {
        this.isTaskFailed = isTaskFailed;
        this.start = range * number;
        this.headers = headers;
    }

    @Override
    public void request(long code) {
        try {
            boolean isTaskFailedLocal = isTaskFailed.get();
            if(isTaskFailedLocal) {
                isTaskFailed.set(false);
                System.err.println("Filed request start again");
            } else {
                System.err.println("Trying to make request");
            }
            URL obj = new URL(link);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setConnectTimeout(TIMEOUT);
            con.setRequestMethod(METHOD);
            headers.forEach(con::setRequestProperty);

            con.setDoOutput(true);
            try(DataOutputStream wr = new DataOutputStream(con.getOutputStream());
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(con.getInputStream()))) {
                String params = "code=" + code;
                wr.writeBytes(params);
                wr.flush();

                String inputLine;
                StringBuilder response = new StringBuilder();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                String res = response.toString();

                if (!res.contains(WRONG_RESULT)) {
                    Files.write(Paths.get(RESULT_FILENAME), Collections.singletonList(res), Charset.forName("UTF-8"));
                }
            } catch (IOException e) {
                System.err.println(e.getMessage());
            }
        } catch (Exception e) {
            System.err.println("Request failed");
            isTaskFailed.set(true);
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
            //TODO listen server (make request every 10sec and if 200 status got try again else sleep for 10sec)
            request(code);
        }
    }

    @Override
    public void run() {
        while(isTaskFailed.get()) {
        }
        while (!isTaskFailed.get()) {
            for (long code = start; code < start + range; code++) {
                try {
                    request(code);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            break;
        }
    }


    public int getRange() {
        return range;
    }

    public void setRange(int range) {
        this.range = range;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public AtomicBoolean getIsTaskFailed() {
        return isTaskFailed;
    }

    public void setIsTaskFailed(AtomicBoolean isTaskFailed) {
        this.isTaskFailed = isTaskFailed;
    }

    public long getStart() {
        return start;
    }

    public void setStart(long start) {
        this.start = start;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }
}
